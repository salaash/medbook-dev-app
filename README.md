## Setup ##

* git clone https://salaash@bitbucket.org/salaash/medbook-dev-app.git
* cd medbook-dev-app/app-files
* create an empty database
* cp .env.example .env
* modify DB_DATABASE, DB_USERNAME, DB_PASSWORD in .env
* php artisan migrate
* composer install
* php artisan key:generate
